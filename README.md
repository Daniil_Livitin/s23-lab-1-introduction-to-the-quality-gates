# Lab 1 -- Introduction to the quality gates
[![pipeline status](https://gitlab.com/Daniil_Livitin/s23-lab-1-introduction-to-the-quality-gates/badges/main/pipeline.svg)](https://gitlab.com/Daniil_Livitin/s23-lab-1-introduction-to-the-quality-gates/-/commits/main)

## Process of auto deployment

1) After each push to main branch new docker image is built and pushed to the [docker hub](https://hub.docker.com/repository/docker/dablup/sqr_labs/general).
2) Next step is run this new image on the remote server using `ssh`.

You can access web page following this link: http://51.250.2.35:8080/hello