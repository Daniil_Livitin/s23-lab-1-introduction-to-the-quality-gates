FROM --platform=linux/amd64 eclipse-temurin:17-jdk
WORKDIR /app
COPY ./target/main-0.0.1-SNAPSHOT.jar /app/app.jar
EXPOSE 8080
ENTRYPOINT [ "java", "-jar", "/app/app.jar" ]